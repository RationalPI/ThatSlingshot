#include "mainwindow.h"
#include <QApplication>
#include <QMessageBox>

#define handleExceptions(tryed)\
	try {\
	tryed\
	}catch (std::string& s) {\
	QMessageBox::critical(nullptr,"Critical failure",QString()+"Error: \""+QString::fromStdString(s)+"\"");\
	exit(EXIT_FAILURE);\
	}catch (const char*& s) {\
	QMessageBox::critical(nullptr,"Critical failure",QString()+"Error: \""+QString::fromStdString(s)+"\"");\
	exit(EXIT_FAILURE);\
	}catch (...) {\
	QMessageBox::critical(nullptr,"Critical failure","Unknown error!");\
	exit(EXIT_FAILURE);\
	}

struct App:public QApplication{
	using QApplication::QApplication;

	bool notify(QObject *o, QEvent *e)override{
		handleExceptions(
					return QApplication::notify(o,e);
				)
				return false;
	}
};

int main(int argc, char *argv[]){
	App a(argc,argv);
	handleExceptions(
			MainWindow w(argc,argv);
			w.showMaximized();
			return a.exec();
			)
}


