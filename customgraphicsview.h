#pragma once
#include <QGraphicsView>

struct CustomGraphicsView:public QGraphicsView{
	Q_OBJECT
public:
	CustomGraphicsView(QWidget *parent = nullptr);
	void wheelEvent(QWheelEvent *event) override;
signals:
	void scaleChanged();
};

