#pragma once

#include <QGraphicsItem>

struct GraphScalesGraphicItem:public QGraphicsItem{
	GraphScalesGraphicItem(double h,double w):h(h),w(w){}
private:
	const double h;
	const double w;
	QRectF boundingRect() const override;
	void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;
};
