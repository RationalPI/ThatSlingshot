#include "curvegraphicitem.h"

#include <QApplication>
#include <QDesktopWidget>
#include <QScreen>
#include <QCursor>
#include <QGraphicsSceneMouseEvent>
#include <QMenu>
#include <array>

#include <mainwindow.h>
#include <customgraphicsview.h>

CurveGraphicItem::CurveGraphicItem(Simulation::SimSetup ss, MainWindow *parent):
	parent(parent),data(ss){
	auto updateScale=[=]{
		auto screenHeight=QApplication::desktop()->screenGeometry(QCursor::pos()).height();
		//		auto screenHeight=QGuiApplication::screenAt(QCursor::pos())->geometry().height();
		auto screenHeightInScene=parent->graphView()->mapToScene(0,screenHeight).ry()-parent->graphView()->mapToScene(0,0).ry();
		selectionMargin=screenHeightInScene*8/1080;
		textHeight=screenHeightInScene*14/1080;
	};
	QObject::connect(parent->graphView(),&CustomGraphicsView::scaleChanged,updateScale);
	updateScale();
	setFlag(QGraphicsItem::ItemIsSelectable,true);
}

void CurveGraphicItem::addSpeedPoint(qreal x, SIM::MetersPerSeconds y){
	data.speedPoints.push_back({x,speedFactor*y});
	auto ammoW=data.ss.ammoMv*(4./3)*3.141592*std::pow(data.ss.ammoDiameter/2,3);
	data.powerPoints.push_back({x,-powerFactor*0.5*ammoW*y*y});
	prepareGeometryChange();
}

void CurveGraphicItem::addForcePoint(qreal x, SIM::Kg y){
	data.forcePoints.push_back({x,forceFactor*y});
	prepareGeometryChange();
}

SIM::MetersPerSeconds CurveGraphicItem::maxSpeed(){
	return -QPolygonF(data.speedPoints).boundingRect().top()/speedFactor;
}

SIM::Kg CurveGraphicItem::maxForce(){
	return -QPolygonF(data.forcePoints).boundingRect().top()/forceFactor;
}

void CurveGraphicItem::setRepport(const QString &value){
	data.repport=value;
	setToolTip(value);
}


QPainterPath CurveGraphicItem::shape() const{
	auto strokFromPoints=[this](const QVector<QPointF>& points){
		QPainterPathStroker pps;
		pps.setWidth(selectionMargin);
		QPainterPath painterPath;
		painterPath.addPolygon(QPolygonF(points));
		auto maxValue=painterPath.boundingRect().topRight();
		QVector<QPointF> line;
		line.append({0,maxValue.y()});
		line.append({maxValue.x(),maxValue.y()});
		painterPath.addPolygon(line);
		return pps.createStroke(painterPath);
	};

	QPainterPath ret;
	ret+=strokFromPoints(data.speedPoints);
	ret+=strokFromPoints(data.powerPoints);
	ret+=strokFromPoints(data.forcePoints);

	return ret;
}

QRectF CurveGraphicItem::boundingRect() const{
	QPolygonF points;
	points.append(data.speedPoints);
	points.append(data.forcePoints);
	points.append(data.powerPoints);
	return points.boundingRect().marginsAdded(QMarginsF(selectionMargin,2*textHeight,selectionMargin,selectionMargin));
}

void CurveGraphicItem::mousePressEvent(QGraphicsSceneMouseEvent *event){
	QMenu m;
	switch (event->button()) {
	case Qt::MouseButton::RightButton:
		QObject::connect(m.addAction("Delete"),&QAction::triggered,[this]{
			parent->removeCurve(this);
		});
		m.exec(QCursor::pos());
		break;
	default:
		break;
	}
	QGraphicsItem::mousePressEvent(event);
}

QString CurveGraphicItem::Label(){
	if(lastRepport()[0]=="<")return "MAX ELONGATION";
	return
			QString::number(maxSpeed(),'f',1)+"m/s "
+QString::number(maxForce(),'f',1)+"Kg "
+QString::number(data.ss.bandCount)+"*("+QString::number(1000*data.ss.forkWidth/data.ss.bandCount)+"|"+QString::number(1000*data.ss.poucheWidth/data.ss.bandCount)+")mm";
}

void CurveGraphicItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget){
	if(!isSelected()){
		painter->setOpacity(0.1);
	}

	auto plot=[=](const QVector<QPointF>& points,int factor,int decimalPoints,QString unit,QColor color){
		auto drawMaxValue=[=](QString txt,QRectF rect, QColor c){
			QPen p(c,0);
			painter->save();
			QFont font = painter->font();
			font.setPixelSize(100);
			painter->setFont(font);

			painter->translate(rect.topLeft().x()+textHeight/4,rect.topLeft().y()-textHeight/4);
			painter->scale(textHeight/100,textHeight/100);

			painter->drawText(0,0,txt);
			painter->restore();

			p.setStyle(Qt::PenStyle::DotLine);
			painter->setPen(p);
			painter->drawLine(rect.topLeft(),rect.topRight());
		};


		QPen pen(color,0/*cosmetic*/);
		painter->setPen(pen);
		auto polygone=QPolygonF(points);
		painter->drawPolyline(polygone);

		auto maxValue=-polygone.boundingRect().topRight().y();
		drawMaxValue(
					QString::number(maxValue/factor,'f',decimalPoints)+unit,
					polygone.boundingRect(),
					color
					);
	};

	plot(data.speedPoints,speedFactor,1,"m/s",Qt::blue);
	plot(data.powerPoints,powerFactor,2,"J",Qt::darkGreen);
	plot(data.forcePoints,forceFactor,1,"Kg",Qt::red);
}
