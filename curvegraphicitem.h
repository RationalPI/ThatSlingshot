#pragma once

#include <QGraphicsItem>
#include <simulator.h>
#include <cereal/cereal.hpp>

struct CurveData{
	CurveData& operator =(const CurveData& other){
		speedPoints=other.speedPoints;
		forcePoints=other.forcePoints;
		powerPoints=other.powerPoints;
		repport=other.repport;
		ss=other.ss;
		return *this;
	}
	//! constructor for serialisation
	CurveData(){}
	//! the main constructor of the class
	CurveData(Simulation::SimSetup ss):ss(ss){}

	template <class Archive>
	void serialize( Archive & ar ,const std::uint32_t version){
		ar(CEREAL_NVP(speedPoints),
			CEREAL_NVP(forcePoints),
			CEREAL_NVP(powerPoints),
			CEREAL_NVP(repport),
			CEREAL_NVP(ss));
	}

	QVector<QPointF> speedPoints;
	QVector<QPointF> forcePoints;
	QVector<QPointF> powerPoints;

	QString repport;
	Simulation::SimSetup ss;
};

struct CurveGraphicItem:public QGraphicsItem{
	CurveGraphicItem(Simulation::SimSetup ss,MainWindow* parent);
	void addSpeedPoint(qreal x, SIM::MetersPerSeconds y);
	void addForcePoint(qreal x, SIM::Kg y);
	SIM::MetersPerSeconds maxSpeed();
	SIM::Kg maxForce();
	void setRepport(const QString& value);
	QString Label();
	const QString& lastRepport()const{return data.repport;};
	const Simulation::SimSetup& setup()const{return data.ss;};
	const CurveData& valueOfdata()const{return data;}
	void setData(const CurveData& d){
		data=d;
		setRepport(data.repport);
	}
private:
	QPainterPath shape() const override;
	QRectF boundingRect() const override;
	void mousePressEvent(QGraphicsSceneMouseEvent *event) override;
	void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;

	static const auto powerFactor=20;
	static const auto speedFactor=4;
	static const auto forceFactor=40;
	double selectionMargin=0;//!< in scene unit so that it show constant in size in the view
	double textHeight=0;//!< in scene unit so that it show constant in size in the view
	MainWindow* parent;
	CurveData data;
};
