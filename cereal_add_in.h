#pragma once

#include <cereal/cereal.hpp>
#include <QVector>
#include <QString>
#include <QPointF>


namespace cereal{

	template <class Archive, class T> inline
	void CEREAL_SAVE_FUNCTION_NAME( Archive & ar, const QVector<T>& vector){
		std::vector<T> QVector;
		std::copy(vector.begin(),vector.end(),std::back_inserter(QVector));
		ar(CEREAL_NVP(QVector));
	}

	template <class Archive, class T> inline
	void CEREAL_LOAD_FUNCTION_NAME( Archive & ar, QVector<T>& vector){
		std::vector<T> QVector;
		ar(CEREAL_NVP(QVector));
		vector.clear();
		std::copy(QVector.begin(),QVector.end(),std::back_inserter(vector));
	}

	///////////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////

	template <class Archive> inline
	void CEREAL_SAVE_FUNCTION_NAME( Archive & ar, const QString& s){
		std::string QString=s.toStdString();
		ar(CEREAL_NVP(QString));
	}

	template <class Archive> inline
	void CEREAL_LOAD_FUNCTION_NAME( Archive & ar, QString& s){
		std::string QString;
		ar(CEREAL_NVP(QString));
		s=QString::fromStdString(QString);
	}

	///////////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////

	template <class Archive> inline
	void CEREAL_SAVE_FUNCTION_NAME( Archive & ar, const QPointF& p){
		qreal x,y;
		x=p.x();
		y=p.y();
		ar(CEREAL_NVP(x),CEREAL_NVP(y));
	}

	template <class Archive> inline
	void CEREAL_LOAD_FUNCTION_NAME( Archive & ar, QPointF& p){
		qreal x,y;
		ar(CEREAL_NVP(x),CEREAL_NVP(y));
		p.setX(x);
		p.setY(y);
	}

	}
