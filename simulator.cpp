#include "simulator.h"

#include "mainwindow.h"

#include <QApplication>
#include <QThread>
#include <array>
#include <QtConcurrentRun>

#include "customgraphicsview.h"
#include "curvegraphicitem.h"


constexpr auto bandLinearAerodynaFriction=0.0000001;
constexpr auto internalFriction=0.005f;
namespace SIM {
	struct Particle{
		struct PysicalStatus{
			PysicalStatus(M pos=0):pos(pos){}
			M pos;//!< the possition in the 1D world
			MetersPerSeconds speed=0;
		};
		Particle():CA(bandLinearAerodynaFriction),mass(0){throw "INVALIDE PARTICLE INITIALISATION";}
		Particle(Kg mass,PysicalStatus initialStatus,AerodynamicDragCoeffFactored ca=bandLinearAerodynaFriction):CA(ca),mass(mass),_status(initialStatus){}
		void addStrain(Newton effect){
			stagedStrain+=effect;
			lastStrain=stagedStrain;
		}
		void resolvStrains(Seconds elapsed){
			auto oldStatus=_status;
			auto frictionlessNewSpeed=elapsed*stagedStrain/mass+oldStatus.speed;

			auto aireFrictionForce=(1./2.)*1*std::pow(frictionlessNewSpeed,2)*CA;
			stagedStrain+=frictionlessNewSpeed>0?-aireFrictionForce:aireFrictionForce;

			_status.speed=elapsed*stagedStrain/mass+oldStatus.speed;
			_status.pos=elapsed*_status.speed+oldStatus.pos;
			stagedStrain=0;
		}
		//! reset all but pos of a particle
		void reset(){
			_status.speed=0;
			stagedStrain=0;
		}
		const PysicalStatus& status()const{
			return _status;
		}
		const Newton& valueOflastStrain()const{
			return lastStrain;
		}
	private:
		const AerodynamicDragCoeffFactored CA;//!< friction Coef factored with section (stremlined body drag)
		const Kg mass;
		PysicalStatus _status;
		Newton stagedStrain=0;
		Newton lastStrain=0;//! equal to stagedStrain but never reset to 0 (for loging puroposes)
	};

	struct Link{
		Link(
				Scene& scene,
				ParticleID& a,
				ParticleID& b,
				M restLenght,
				M restWidth,
				const std::function<NewtonPerM(Elongation)>& strainCurve
				):scene(scene),a(a),b(b),restLenght(restLenght),restWidth(restWidth),strainCurve(strainCurve)
		{}
		void act(Seconds elapsed);
		Elongation currentElongation()const;
	private:
		Scene& scene;
		ParticleID a;//!< particle linked to on end of the spring
		ParticleID b;//!< particle linked to the other end of the spring
		Elongation lastElong=0;//! len at last action (used to determine internal friction effect)
		const M restLenght;//!< the lenth at rest of the spring
		const M restWidth;//!< the width of the spring
		const std::function<NewtonPerM(Elongation)>& strainCurve;//! the strain curve per width in meter of that kind of spring
		const float internalFrictionFactor=internalFriction;
	};

	struct Scene{
		Particle& operator[](const ParticleID& id){
			if(dynamicParticles.count(id)){
				return dynamicParticles[id];
			}else if(staticParticles.count(id)){
				return staticParticles[id];
			}else if(dynamicMasterParticles.count(id)){
				return dynamicMasterParticles[id];
			}else{
				throw "BAD PARTICLE ID IN SCENE";
			}
		}
		void stepInTime(Seconds elapsed){
			for (auto& link : links) {
				link.act(elapsed);
			}
			for (auto& dynamicParticle  : dynamicParticles) {
				dynamicParticle.second.resolvStrains(elapsed);
			}
			for (auto& dynamicParticle  : dynamicMasterParticles) {
				dynamicParticle.second.resolvStrains(elapsed);
			}
		}
		//! play the simulation but make the master particles static for that time
		void stabiliseMasterParticles(Seconds stabilisingTimestep,uint64_t stabilisingSteps,bool resetStatus=true){
			for (int i = 0; i < stabilisingSteps; ++i) {
				for (auto& link : links) {
					link.act(stabilisingTimestep);
				}
				for (auto& dynamicParticle  : dynamicParticles) {
					dynamicParticle.second.resolvStrains(stabilisingTimestep);
				}
			}

			if(resetStatus){
				for (auto& particle : staticParticles) {
					particle.second.reset();
				}
				for (auto& particle : dynamicParticles) {
					particle.second.reset();
				}
				for (auto& particle : dynamicMasterParticles) {
					particle.second.reset();
				}
			}
		}
		ParticleID addStaParticle(Particle&& p){
			staticParticles.insert({pID,std::move(p)});
			return pID++;
		}
		ParticleID addDynParticle(Particle&& p){
			dynamicParticles.insert({pID,std::move(p)});
			return pID++;
		}
		ParticleID addDynMasterParticle(Particle&& p){
			dynamicMasterParticles.insert({pID,std::move(p)});
			return pID++;
		}
		LinkIndex addLink(Link&& l){
			links.push_back(std::move(l));
			return links.size()-1;
		}
		const std::vector<Link>& valueOflinks()const{return links;}
	private:
		Particles staticParticles;
		Particles dynamicParticles;
		Particles dynamicMasterParticles;
		std::vector<Link> links;
		ParticleID pID=0;
	};

	}


void SIM::Link::act(SIM::Seconds elapsed){
	auto elongation=currentElongation();
	auto elongationStrain=restWidth*strainCurve(elongation);

	auto deltaElongationPerSecond=(elongation-lastElong)/elapsed;

	auto internalFricitonStrain=restWidth*deltaElongationPerSecond*internalFrictionFactor;
	lastElong=elongation;

	auto bounded=[](float a,float val,float b){
		float ret;

		if((a<=val&&val<=b)||(a>=val&&val>=b)){
			ret = val;
		}else if(a>=b){
			if(val>=a)ret = a;
			else ret =  b;
		}else/*a<b*/{
			if(val>=b)ret =  b;
			else ret =  a;
		}

		return ret;
	};
	auto strain=bounded(0,elongationStrain+internalFricitonStrain,elongationStrain);

	auto& _a=scene[a];
	auto& _b=scene[b];
	if(_a.status().pos>_b.status().pos){
		_a.addStrain(-strain);
		_b.addStrain(strain);
	}else{
		_a.addStrain(strain);
		_b.addStrain(-strain);
	}
}

SIM::Elongation SIM::Link::currentElongation() const{
	auto currentLen=std::abs(scene[a].status().pos-scene[b].status().pos);
	return (currentLen-restLenght)/restLenght;
}

constexpr auto tbgKgPm2=6.3/(47*210/1000);//!< teraband gold  Kg/m2
//! newton per width (metters) per elongation of teraband gold
std::function<SIM::NewtonPerM(SIM::Elongation)> tbgNPerElong=[](float a){
	/*

		official data for 125mm wide band
		25	%	7.9	pounds
		50	%	13.9	pounds
		75	%	18.1	pounds
		100	%	21.6	pounds
		125	%	24.6	pounds
		150	%	27.5	pounds
		175	%	30.3	pounds
		200	%	33.4	pounds
		225	%	36.6	pounds
		250	%	40.1	pounds

	auto terabandGold=std::vector<std::pair<double,double>>{
		{0,3.5},//8mm bb 2.09g , cm mesured # that's the 0% elongation point
		{10,3.7},
		{20,4},
		{30,4.15},
		{40,4.55},
		{50,5},
		{60,5.3},
		{70,5.7},
		{80,6.5},
		{90,7},
		{100,7.6},
		{110,8.5},
		{120,9.1},
		{130,9.7},
		{140,10.6},
		{150,11.4},
		{160,12},
		{170,12.7},
		{180,13.4},
		{190,14.1},
		{200,15.5},
		{210,16.6},
		{220,17.2},
		{230,17.5},
		{240,17.8},
		{250,18},
		{260,18.3},
		{270,18.4},
		{280,18.5}
	};

		this polynom is 100% elongation at 90bb*2.09g so 1,84463087038698 newtons
		the official data tells that its a 21.6 pounds so 96,08158656 newtons
		so our mesured curve is 0.019198588787198 times as powerfull than real
		f(x) = 2.53955389E-10 *x^5 - 2.6157200273E-07 *x^4 + 9.8392303312E-05 *x^3 - 0.0169053771917 *x^2 + 1.8396958434308 x - 1.0604544386532
		R2 = 0.999166550465818
	*/

	static constexpr auto subIntegerPrecision=20.f;
	static auto lut=[]{
		static constexpr auto count=700*int(subIntegerPrecision);
		std::array<float,count> ret;

		static const auto coefs=std::vector<double>({
																	  -1.0604544386532		,
																	  +1.8396958434308		,//x
																	  -0.0169053771917		,//*x^2
																	  +9.8392303312E-05	,//*x^3
																	  -2.6157200273E-07	,//*x^4
																	  2.53955389E-10		,//*x^5
																  });
		constexpr float calibration=0.5f*33.4f/3.94968f;// calibrated with officlia teraband marketing (marketed for 12" wide band)

		//https://www.youtube.com/watch?v=QFDKvGig-l4
		// 12 pound at 47 inches with 20 cm band 2* 25/20
		// 22 pound at 43 inches with 20 cm band 4* 25/20
		// 35 pound at 35 inches with 20 cm band 6* 25/20


		for (int i = 0; i < count; ++i) {
			float factor = calibration;
			float result = 0;
			for(int term = 0; term < 6; term++) {
				result += coefs[term] * factor;
				factor *= i/subIntegerPrecision;/*per one to %*/
			}
			ret[i]=result;
		}

		return ret;
	}();


	auto x=a*100*subIntegerPrecision;
	if(x>=0){
		if(x>lut.size()-1)return *lut.rbegin();
		return lut[int(x)];
	}else{
		return 0.f;
	}
};
constexpr auto timeStep=0.000008f;

bool Simulation::isEnded(){
	return results.isEnded()&&launched;
}

template<class T,class U> inline
U mapRange(const T& valueA,const T& minA,const T& maxA,const U& minB,const U& maxB,const U& outOfRangeMin={},const U& outOfRangeMax={}){
	if(valueA<minA)return outOfRangeMin;
	if(valueA>maxA)return outOfRangeMax;
	auto aRatio=(valueA-minA)/(maxA-minA);
	return minB+aRatio*(maxB-minB);
}

Simulation::Simulation(MainWindow *parent, Simulation::SimSetup ss){
	//	auto newtond=SIM::inches(6)*tbgNPerElong(250./100);/*test For calibration*/

	scene=std::make_shared<SIM::Scene>();
	auto& scene=*this->scene;
	auto ammoWeight=ss.ammoMv*(4./3)*3.141592*std::pow(ss.ammoDiameter/2,3);

	const auto bandSegmentRelaxedLen=ss.bandLen/ss.bandDiscretisationCount;
	std::map<SIM::ParticleID,std::vector<SIM::LinkIndex>> linksAttachedToParticles;
	/*init scenes*/{

		auto anchorID=scene.addStaParticle(SIM::Particle(0,{}));
		patricleItems.insert({anchorID,new QGraphicsRectItem(0,0,1,1000*ss.forkWidth)});
		SIM::ParticleID previousID=anchorID;
		for (int i = 0; i < ss.bandDiscretisationCount-1; ++i) {
			auto width=ss.forkWidth*((ss.bandDiscretisationCount-i)/ss.bandDiscretisationCount)+ss.poucheWidth*(i/ss.bandDiscretisationCount);
			auto surfaceArea=width*bandSegmentRelaxedLen;
			auto newID=scene.addDynParticle(SIM::Particle(tbgKgPm2*surfaceArea,SIM::Particle::PysicalStatus((i+1)*ss.drawLen/ss.bandDiscretisationCount)));
			patricleItems.insert({newID,new QGraphicsRectItem(0,0,
										 1000*bandSegmentRelaxedLen,
										 ss.forkWidth*1000*((ss.bandDiscretisationCount-i)/ss.bandDiscretisationCount)+ss.poucheWidth*1000*(i/ss.bandDiscretisationCount)
										 )});

			auto linkIndex=scene.addLink(SIM::Link(
													  scene,
													  previousID,
													  newID,
													  bandSegmentRelaxedLen,
													  width,
													  tbgNPerElong
													  ));
			linksAttachedToParticles[previousID].push_back(linkIndex);
			linksAttachedToParticles[newID].push_back(linkIndex);

			previousID=newID;
		}
		auto sphereAndPoucheDragCoef=[](float diameter){
			return 2*3.141592*std::pow(diameter/2,2);
			//https://en.wikipedia.org/wiki/Drag_coefficient
		};

		lastID=scene.addDynMasterParticle(
					SIM::Particle(
						ss.poucheWeight+ammoWeight,
						SIM::Particle::PysicalStatus(ss.drawLen),
						sphereAndPoucheDragCoef(ss.ammoDiameter)
						)
					);
		patricleItems.insert({lastID,new QGraphicsEllipseItem(0,0,1000*ss.ammoDiameter,1000*ss.ammoDiameter)});

		auto linkIndex=scene.addLink(SIM::Link(
												  scene,
												  previousID,
												  lastID,
												  bandSegmentRelaxedLen,
												  ss.poucheWidth,
												  tbgNPerElong
												  ));
		linksAttachedToParticles[previousID].push_back(linkIndex);
		linksAttachedToParticles[lastID].push_back(linkIndex);
	}

	// filling the scene with items
	parent->simulationScene()->clear();
	for (auto& patricleItem : patricleItems) {
		QPen p(Qt::black);
		p.setWidth(0/*decorative*/);
		patricleItem.second->setPen(p);
		parent->simulationScene()->addItem(patricleItem.second);
	}

	auto curve=std::make_shared<CurveGraphicItem>(ss,parent);
	parent->addCurve(curve);

	static const uint8_t logIntervale=10;
	static const auto maxSimSteps=0.1/timeStep;/*0.1 seconds*/

	QtConcurrent::run([this,&scene,linksAttachedToParticles]{
		auto mkFrame=[this,&scene,linksAttachedToParticles]{
			Results::Frame frame;
			const auto& links=scene.valueOflinks();

			for (auto& patricleItem : patricleItems) {
				auto& particle=scene[patricleItem.first];
				auto& particleState=frame[patricleItem.first];

				particleState.force=particle.valueOflastStrain();
				particleState.pos=particle.status().pos;
				particleState.speed=particle.status().speed;
				SIM::Elongation elongation=0;
				auto& linksAttachedToParticle=linksAttachedToParticles.at(patricleItem.first);
				for (auto& linkAttachedIndex : linksAttachedToParticle) {
					elongation+=links[linkAttachedIndex].currentElongation();
				}
				particleState.elongation=elongation/linksAttachedToParticle.size();
			}

			return frame;
		};

		/*stabilizing rubber by letting it settle*/{
			auto coarsTimeStep=timeStep*4;
			dynamic_cast<QGuiApplication*>(QGuiApplication::instance())->setOverrideCursor(Qt::WaitCursor);
			scene.stabiliseMasterParticles(coarsTimeStep,10.f/coarsTimeStep);
			dynamic_cast<QGuiApplication*>(QGuiApplication::instance())->restoreOverrideCursor();
		}

		while (simStepCount<maxSimSteps) {
			scene.stepInTime(timeStep);
			//			scene.stabiliseMasterParticles(timeStep,1,false);

			if(simStepCount++%logIntervale==0){
				results.putFrame(mkFrame());
			}
		}
		results.stop();
	});


	if(ss.slowTimeFactor>=100) throw "slowTimeFactor can't be greater that 99";
	viewTimer.setSingleShot(false);
	viewTimer.setInterval(ss.slowTimeFactor);
	connect(&viewTimer,&QTimer::timeout,[this,ammoWeight,parent,curve,ss]{
		try {

			Results::FrameIndex frameIdx;
			if(ss.slowTimeFactor<0.9/*freez the first frames if a slow time factor is applied*/){
				frameIdx=drawStepCount++;
			}else{
				/*ignor and increment until 50 then use minus 50*/
				if(drawStepCount>=50){
					frameIdx=drawStepCount++-50;
				}else{
					drawStepCount++;
					frameIdx=0;
					elapsed=0;
				}
			}

			auto& frame=results.valueOfFrame(frameIdx);
			auto& poucheParticleState=frame.at(lastID);

			if(!launched){
				curve->addSpeedPoint(10000*elapsed,poucheParticleState.speed);
				curve->addForcePoint(10000*elapsed,poucheParticleState.force/9.80665);

				if(poucheParticleState.pos<0){
					launched=true;
					SIM::MetersPerSeconds maxSpeed=curve->maxSpeed();
					SIM::Kg maxStrainOnPouche=curve->maxForce();

					auto maxElong=results.valueOfFrame(0).at(lastID).elongation;
					QString repport=QString()
							+"Max elongation "+QString::number(100.*maxElong,'f',0)+" %\n"
							+"Power "+QString::number(0.5*ammoWeight*maxSpeed*maxSpeed,'f',3)+" J\n"
							+"\n"
							+"Speed "+QString::number(maxSpeed,'f',0)+" m/s\n"
							+"Draw "+QString::number(maxStrainOnPouche,'f',2)+" Kg\n"
							+"\n"
							+"Speed "+QString::number(maxSpeed*3.28083989501312,'f',0)+" fps\n"
							+"Draw "+QString::number(maxStrainOnPouche*9.80665*0.22480894244319,'f',2)+" lbs\n"
							+"\n"
							+"Ammo "+QString::number(ammoWeight*1000,'f',2)+" g\n"
							+"Time "+QString::number(elapsed,'f',3)+" secondes\n"
							;

					if(maxElong>=ss.maxElong){
						repport="<font color=\"red\"><b>MAX ELONGATION "+QString::number(100.*maxElong,'f',0)+"%</b></font><br>";
						repport+=repport;
						repport+=repport;
						repport+=repport;
						repport+=repport;
						repport+=repport;
					}

					curve->setRepport(repport);
					parent->updateLabelsAndRepport();
				}
			}

			if(ss.slowTimeFactor!=0||launched){
				for (auto& patricleItem : patricleItems) {
					static const auto low	=240/*blue*/;
					static const auto high	=90/*yellow ish*/;
					auto& particleItem=patricleItem.second;
					auto& particleState=frame.at(patricleItem.first);

					particleItem->setPos(particleState.pos*1000,0);
					auto elongationHue=mapRange<float,int>(particleState.elongation,0,ss.maxElong,low,high,low,low);
					particleItem->setPen(QColor::fromHsv(elongationHue,255,255));
				}
			}

			elapsed+=timeStep*logIntervale;
		} catch (Results::NoMoreData) {
			viewTimer.stop();
		}
	});
	viewTimer.start();
}

const Simulation::Results::Frame &Simulation::Results::valueOfFrame(Simulation::Results::FrameIndex frameID){
	while (lastFrameIdx<frameID||lastFrameIdx==uint64_t(-1)) /*wait until the frame is disponible*/{
		if(ended)throw NoMoreData();
		QThread::msleep(1);
	}

	return *frames[frameID];
}
