#pragma once

#include <QMainWindow>
#include <QGraphicsScene>
#include <QTextEdit>

#include "simulator.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

struct CurveGraphicItem;
struct CustomGraphicsView;
struct Simulation;
class MainWindow : public QMainWindow{
public:
	MainWindow(int argc, char *argv[]);
	~MainWindow();

	//! curve will be owned by the curve liste widget and by simu as a sPtr,
	//! it will be in the scene at the same time, use MaiWindow::removeCurve
	//! to remove it from the liste and scene,
	//! it will still be owned by the current simulation until its destruction
	void addCurve(std::shared_ptr<CurveGraphicItem> curve);
	//! remove the curve from the liste and scene,
	//! it will still be owned by the current simulation until its destruction
	void removeCurve(std::shared_ptr<CurveGraphicItem> curve);
	//! remove the curve from the liste and scene,
	//! it will still be owned by the current simulation until its destruction
	void removeCurve(CurveGraphicItem* curve);
	//! set the current selection in the graph view and in the liste view simultaneously
	void setSelected(std::shared_ptr<CurveGraphicItem> curve={});

	QGraphicsScene* simulationScene();
	CustomGraphicsView* graphView();
	//! set repport and update liste labels
	void updateLabelsAndRepport();

private:
	Ui::MainWindow *ui;
	std::shared_ptr<Simulation> simu;
	std::vector<QMetaObject::Connection> connectionToDisableBeforDeletingUI;
	friend Simulation;
};
