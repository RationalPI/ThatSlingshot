#include "mainwindow.h"
#include "./ui_mainwindow.h"

#include "simulator.h"
#include "graphscalesgraphicitem.h"
#include "curvegraphicitem.h"
#include <QDesktopWidget>
#include <QFileDialog>

#include <cereal/archives/json.hpp>
#include <cereal/types/vector.hpp>
#include <cereal_add_in.h>
#include <fstream>

struct CurveListeItem:public QListWidgetItem{
	CurveListeItem(QString s,std::shared_ptr<CurveGraphicItem> curve):QListWidgetItem(s),curve(curve){}
	std::shared_ptr<CurveGraphicItem> curve;//!< owned by the scene
};

MainWindow::MainWindow(int argc, char *argv[])
	: QMainWindow(nullptr)
	, ui(new Ui::MainWindow)
{
	/*objects initialisations*/{
		ui->setupUi(this);

		for(auto& bt:{ui->tbt_swipe_ammoDia,ui->tbt_swipe_bl,ui->tbt_swipe_dl,ui->tbt_swipe_fbw,ui->tbt_swipe_pbw}){
			bt->setIcon(QApplication::style()->standardIcon(QStyle::SP_MediaSkipBackward));
		}
		for(auto& bt:{ui->tbt_swipe_ammoDiap,ui->tbt_swipe_blp,ui->tbt_swipe_dlp,ui->tbt_swipe_fbwp,ui->tbt_swipe_pbwp}){
			bt->setIcon(QApplication::style()->standardIcon(QStyle::SP_BrowserReload));
		}
		for(auto& bt:{ui->tbt_swipe_ammoDiapp,ui->tbt_swipe_blpp,ui->tbt_swipe_dlpp,ui->tbt_swipe_fbwpp,ui->tbt_swipe_pbwpp}){
			bt->setIcon(QApplication::style()->standardIcon(QStyle::SP_MediaSkipForward));
		}

		ui->Play->setIcon(QApplication::style()->standardIcon(QStyle::SP_MediaPlay));
		ui->bt_play->setIcon(QApplication::style()->standardIcon(QStyle::SP_MediaPlay));
		ui->clear_All->setIcon(QApplication::style()->standardIcon(QStyle::SP_DialogDiscardButton));

		ui->simulationView->setScene(new QGraphicsScene());
		ui->simulationView->setRenderHints(QPainter::RenderHint::Antialiasing);

		ui->graphView->setScene(new QGraphicsScene());
		ui->graphView->setRenderHints(QPainter::RenderHint::Antialiasing);

		auto scale=new GraphScalesGraphicItem(400,150);
		ui->graphView->scene()->addItem(scale);
		auto scaleFactor=0.7*QApplication::desktop()->screenGeometry(QCursor::pos()).height()/400;
		ui->graphView->scale(scaleFactor,scaleFactor);

		ui->curveListWidget->setContextMenuPolicy(Qt::ContextMenuPolicy::CustomContextMenu);
	}

	/*actions that start simuations*/{
		auto simSetupFromGUI=[this]{
			Simulation::SimSetup ss;

			ss.slowTimeFactor=ui->slowTimeFactor->value();
			ss.bandLen=ui->bandLen->value()/1000;
			ss.drawLen=ui->drawLen->value()/1000;
			ss.forkWidth=ui->bandcount->value()*ui->febw->value()/1000;
			ss.poucheWidth=ui->bandcount->value()*ui->pebw->value()/1000;
			ss.bandCount=ui->bandcount->value();
			ss.ammoDiameter=ui->ammoDia->value()/1000;
			ss.ammoMv=ui->ammoMV->value();
			ss.maxElong=ui->maxElong->value()/100;
			ss.poucheWeight=ui->poucheWeight->value()/1000;

			return ss;
		};

		auto playCurentSimSetup=[=](){
			simu=std::make_shared<Simulation>(this,simSetupFromGUI());
		};

		connect(ui->Play,&QAction::triggered,playCurentSimSetup);
		connect(ui->bt_play,&QAbstractButton::clicked,playCurentSimSetup);


		/*swipes*/{
			auto batchSimulate=[this](const Simulation::SimSetup& ss){
				simu=std::make_shared<Simulation>(this,ss);
				while (!simu->isEnded()) {
					QApplication::instance()->processEvents();
				}
			};
			auto swipeDown=[this,batchSimulate](const Simulation::SimSetup& ss,float& ss_swipedValue,float pitch){
				auto ss_swipedValueBak=ss_swipedValue;

				ss_swipedValue=ss_swipedValueBak-3*pitch;batchSimulate(ss);
				ss_swipedValue=ss_swipedValueBak-2*pitch;batchSimulate(ss);
				ss_swipedValue=ss_swipedValueBak-1*pitch;batchSimulate(ss);

				auto row=ui->curveListWidget->currentRow();
				auto originalCurve=ui->curveListWidget->takeItem(row-3);
				ui->curveListWidget->insertItem(row,originalCurve);
				ui->curveListWidget->setCurrentRow(row-3);
			};
			auto swipe=[this,batchSimulate](const Simulation::SimSetup& ss,float& ss_swipedValue,float pitch){
				auto ss_swipedValueBak=ss_swipedValue;

				ss_swipedValue=ss_swipedValueBak-2*pitch;batchSimulate(ss);
				ss_swipedValue=ss_swipedValueBak-1*pitch;batchSimulate(ss);
				ss_swipedValue=ss_swipedValueBak+1*pitch;batchSimulate(ss);
				ss_swipedValue=ss_swipedValueBak+2*pitch;batchSimulate(ss);

				auto row=ui->curveListWidget->currentRow();
				auto originalCurve=ui->curveListWidget->takeItem(row-4);
				ui->curveListWidget->insertItem(row-2,originalCurve);
				ui->curveListWidget->setCurrentItem(originalCurve);
			};
			auto swipeUp=[batchSimulate](const Simulation::SimSetup& ss,float& ss_swipedValue,float pitch){
				auto ss_swipedValueBak=ss_swipedValue;

				ss_swipedValue=ss_swipedValueBak+1*pitch;batchSimulate(ss);
				ss_swipedValue=ss_swipedValueBak+2*pitch;batchSimulate(ss);
				ss_swipedValue=ss_swipedValueBak+3*pitch;batchSimulate(ss);
			};


#define makeConnections(button,buttonp,buttonpp,ssMember,pitch)\
	connect(button,&QAbstractButton::clicked,[simSetupFromGUI,swipeDown]{\
	auto ss=simSetupFromGUI();\
	swipeDown(ss,ssMember,pitch);\
		});\
	connect(buttonp,&QAbstractButton::clicked,[simSetupFromGUI,swipe]{\
	auto ss=simSetupFromGUI();\
	swipe(ss,ssMember,pitch);\
		});\
	connect(buttonpp,&QAbstractButton::clicked,[simSetupFromGUI,swipeUp]{\
	auto ss=simSetupFromGUI();\
	swipeUp(ss,ssMember,pitch);\
		});

			makeConnections(ui->tbt_swipe_ammoDia,ui->tbt_swipe_ammoDiap,ui->tbt_swipe_ammoDiapp,ss.ammoDiameter,SIM::milliMeters(0.5));
			makeConnections(ui->tbt_swipe_bl,ui->tbt_swipe_blp,ui->tbt_swipe_blpp,ss.bandLen,SIM::milliMeters(2));
			makeConnections(ui->tbt_swipe_dl,ui->tbt_swipe_dlp,ui->tbt_swipe_dlpp,ss.drawLen,SIM::milliMeters(5));
			makeConnections(ui->tbt_swipe_fbw,ui->tbt_swipe_fbwp,ui->tbt_swipe_fbwpp,ss.forkWidth,SIM::milliMeters(0.5)*ss.bandCount);
			makeConnections(ui->tbt_swipe_pbw,ui->tbt_swipe_pbwp,ui->tbt_swipe_pbwpp,ss.poucheWidth,SIM::milliMeters(0.5)*ss.bandCount);

#undef makeConnections

		}
	}

	/*actions related to curves (selection deletion context menus ...)*/{
		connect(ui->delCurve,&QAction::triggered,[this]{
			if(ui->curveListWidget->count()){
				auto currentRow=ui->curveListWidget->currentRow();
				removeCurve(static_cast<CurveListeItem*>(ui->curveListWidget->item(currentRow))->curve);
			}
		});

		connect(ui->clear_All,&QAbstractButton::clicked,[this]{
			auto a=ui->graphView->scene()->blockSignals(true);
			auto b=ui->curveListWidget->blockSignals(true);{

				while (ui->curveListWidget->count()) {
					removeCurve(static_cast<CurveListeItem*>(ui->curveListWidget->item(0))->curve);
				}

			}ui->graphView->scene()->blockSignals(a);
			ui->curveListWidget->blockSignals(b);
			setSelected();
		});

		connectionToDisableBeforDeletingUI.push_back(connect(ui->graphView->scene(),&QGraphicsScene::selectionChanged,[this](){
			for (int i = 0; i < ui->curveListWidget->count(); ++i) {
				auto item=static_cast<CurveListeItem*>(ui->curveListWidget->item(i));
				if(item->curve->isSelected()){
					setSelected(item->curve);
					return;
				}
			}
			setSelected();
		}));

		connectionToDisableBeforDeletingUI.push_back(connect(ui->curveListWidget,&QListWidget::currentRowChanged,[this]{
			if(auto item=dynamic_cast<CurveListeItem*>(ui->curveListWidget->currentItem())){
				setSelected(item->curve);
				return;
			}
			setSelected();
		}));

		connect(ui->curveListWidget,&QListWidget::customContextMenuRequested,[this]{
			if(auto item=dynamic_cast<CurveListeItem*>(ui->curveListWidget->currentItem())){
				QMenu m;
				QObject::connect(m.addAction("Delete"),&QAction::triggered,[=]{
					removeCurve(item->curve);
				});
				m.exec(QCursor::pos());
			}
		});
	}

	/*file IO and load file from console arg 2*/{
		connect(ui->actionSave,&QAction::triggered,[this](){
			auto fileName=QFileDialog::getSaveFileName(this,"Select a destination file","","ThatSlinghshot Project (*.tsp)");
			if(!fileName.isEmpty()){
				std::ofstream os(fileName.toStdString(), std::ios::out);
				cereal::JSONOutputArchive archive( os );

				std::vector<CurveData> projectDatas;

				for (int i = 0; i < ui->curveListWidget->count(); ++i) {
					auto item=static_cast<CurveListeItem*>(ui->curveListWidget->item(i));
					projectDatas.push_back(item->curve->valueOfdata());
				}
				archive( CEREAL_NVP(projectDatas) );
			}
		});

		auto loadFile=[this](QString filename){
			if(QFileInfo(filename).exists()){
				std::vector<CurveData> projectDatas;
				/*archive RAII*/{
					std::ifstream os(filename.toStdString(), std::ios::in);
					cereal::JSONInputArchive archive( os );
					archive( CEREAL_NVP(projectDatas) );
				}


				for (auto& projectData : projectDatas) {
					auto curve=std::make_shared<CurveGraphicItem>(projectData.ss,this);
					curve->setData(projectData);
					addCurve(curve);
				}
			}
		};

		connect(ui->actionLoad,&QAction::triggered,[this,loadFile](){
			auto fileName=QFileDialog::getOpenFileName(this,"Select a project file","","ThatSlinghshot Project (*.tsp)");
			loadFile(fileName);
		});

		if(argc>=2){
			loadFile(QString::fromLatin1(argv[1]));
		}
	}
}

MainWindow::~MainWindow(){
	for (auto& connection : connectionToDisableBeforDeletingUI) {
		/*stoping selection change that want to refer to deleted objects at ui deletion*/
		disconnect(connection);
	}

	delete ui;
}

void MainWindow::addCurve(std::shared_ptr<CurveGraphicItem> curve){
	// ui->curveListWidget::CurveListeItem and simulation update log lambda are the curve owners

	auto a=ui->graphView->scene()->blockSignals(true);
	auto b=ui->curveListWidget->blockSignals(true);{

		ui->graphView->scene()->addItem(curve.get());
		ui->curveListWidget->insertItem(ui->curveListWidget->currentRow()+1,new CurveListeItem(curve->Label(),curve));

	}ui->graphView->scene()->blockSignals(a);
	ui->curveListWidget->blockSignals(b);
	setSelected(curve);/*select the item in the list and view*/
}

void MainWindow::removeCurve(std::shared_ptr<CurveGraphicItem> curve){
	removeCurve(curve.get());
}

void MainWindow::removeCurve(CurveGraphicItem *curve){
	ui->graphView->scene()->removeItem(curve);
	for (int i = 0; i < ui->curveListWidget->count(); ++i) {
		auto item=static_cast<CurveListeItem*>(ui->curveListWidget->item(i));
		if(item->curve.get()==curve) delete ui->curveListWidget->takeItem(i);
	}
}

void MainWindow::setSelected(std::shared_ptr<CurveGraphicItem> curve){
	auto a=ui->graphView->scene()->blockSignals(true);
	auto b=ui->curveListWidget->blockSignals(true);{

		ui->graphView->scene()->clearSelection();
		if(curve){
			curve->setSelected(true);
		}

		ui->curveListWidget->setCurrentRow(-1);
		for (int i = 0; i < ui->curveListWidget->count(); ++i) {
			auto item=static_cast<CurveListeItem*>(ui->curveListWidget->item(i));
			if(item->curve==curve) ui->curveListWidget->setCurrentRow(i);
		}


	}ui->graphView->scene()->blockSignals(a);
	ui->curveListWidget->blockSignals(b);

	if(curve){
		/*restor setup*/{
			auto& ss=curve->setup();
			ui->bandLen->setValue(1000*ss.bandLen);
			ui->drawLen->setValue(1000*ss.drawLen);
			ui->febw->setValue(1000*ss.forkWidth/ss.bandCount);
			ui->pebw->setValue(1000*ss.poucheWidth/ss.bandCount);
			ui->bandcount->setValue(ss.bandCount);
			ui->ammoDia->setValue(1000*ss.ammoDiameter);
			ui->ammoMV->setValue(ss.ammoMv);
			ui->maxElong->setValue(ss.maxElong*100);
			ui->poucheWeight->setValue(ss.poucheWeight*1000);
		}
	}
	updateLabelsAndRepport();
}

QGraphicsScene* MainWindow::simulationScene(){return ui->simulationView->scene();}

CustomGraphicsView* MainWindow::graphView(){return ui->graphView;}

void MainWindow::updateLabelsAndRepport(){
	for (int i = 0; i < ui->curveListWidget->count(); ++i) {
		auto item=static_cast<CurveListeItem*>(ui->curveListWidget->item(i));
		item->setText(item->curve->Label());
		if(i==ui->curveListWidget->currentRow()){
			ui->textEdit->setText(item->curve->lastRepport());
		}
	}
}

