#include "customgraphicsview.h"

#include <QWheelEvent>
#include <QScrollBar>


CustomGraphicsView::CustomGraphicsView(QWidget *parent):QGraphicsView(parent){}

constexpr auto scrollFactor = 80;
constexpr auto maxScenePixelSize = 1000;
void CustomGraphicsView::wheelEvent(QWheelEvent *event){
	if(event->modifiers()&Qt::ControlModifier){
		auto scale=event->angleDelta().y()<0?0.9:1.1;
		auto scenePixelSize=1.0/(mapToScene(1,1)-mapToScene(0,0)).x();
		if(
				scale>1/*zoom*/
				&&
				scenePixelSize>maxScenePixelSize
				)
			scale=1;

		// IS == In Scene
		// IV == In View

		auto mousePoseIS=mapToScene(event->pos());
		auto initialMpIV=mapFromScene(mousePoseIS);
		QGraphicsView::scale(scale,scale);
		auto finalMpIV=mapFromScene(mousePoseIS);

		//! translation vector btw old curso pos ans new cursor pos
		auto transIS=mapToScene(finalMpIV)-mapToScene(initialMpIV);

		auto hSBsizeIV=horizontalScrollBar()->size().height();
		auto vSBsizeIV=verticalScrollBar()->size().width();
		auto currentCenterPosIS=
				mapToScene(QPoint(size().width()-hSBsizeIV,size().height()-vSBsizeIV)/2.0);

		//correcting currentCenterPosIS to overlap finalMpIV and initialMpIV
		centerOn(currentCenterPosIS+transIS);
		scaleChanged();
	}else{
		if(event->modifiers()&Qt::ShiftModifier){
			event->angleDelta().y()<0?
						horizontalScrollBar()->setValue( horizontalScrollBar()->value() + scrollFactor ):
						horizontalScrollBar()->setValue( horizontalScrollBar()->value() + -scrollFactor );
		}else{
			event->angleDelta().y()<0?
						verticalScrollBar()->setValue( verticalScrollBar()->value() + scrollFactor ):
						verticalScrollBar()->setValue( verticalScrollBar()->value() + -scrollFactor );
		}
	}
}
