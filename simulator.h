#pragma once

#include <stdint.h>
#include <map>
#include <QObject>
#include <QTimer>
#include <QElapsedTimer>
#include <memory>
#include <cereal/cereal.hpp>

namespace SIM {
	struct Particle;
	struct Scene;
	using ParticleID=uint16_t;
	using LinkIndex=uint16_t;

	using Particles=std::map<ParticleID,Particle>;

	using NewtonPerM=float;
	using Newton=float;
	using Kg=float;
	using M=float;
	using MetersPerSeconds=float;
	using Seconds=float;
	//! Newton per elongation factor (currentLenght/restLenght)
	using Elongation=float;
	//! friction Coef factored with section
	using AerodynamicDragCoeffFactored=double;

	constexpr M milliMeters(float mm){
		return mm/1000.;
	}

	constexpr M inches(float mm){
		return mm*0.0254;
	}

	constexpr Kg grames(float g){
		return g/1000.;
	}

	}

class MainWindow;
class QAbstractGraphicsShapeItem;
struct CurveGraphicItem;


struct Simulation:public QObject{
	struct SimSetup{
		template <class Archive>
		void serialize( Archive & ar ,const std::uint32_t version){
			ar(CEREAL_NVP(bandLen),
				CEREAL_NVP(drawLen),
				CEREAL_NVP(forkWidth),
				CEREAL_NVP(poucheWidth),
				CEREAL_NVP(bandCount),
				CEREAL_NVP(ammoDiameter),
				CEREAL_NVP(ammoMv),
				CEREAL_NVP(slowTimeFactor)
				);
			if(version>0) ar(CEREAL_NVP(maxElong));
			if(version>1) ar(CEREAL_NVP(poucheWeight));
		}
		SimSetup& operator =(const SimSetup& other){
			bandLen=other.bandLen;
			drawLen=other.drawLen;
			forkWidth=other.forkWidth;
			poucheWidth=other.poucheWidth;
			bandCount=other.bandCount;
			ammoDiameter=other.ammoDiameter;
			ammoMv=other.ammoMv;
			slowTimeFactor=other.slowTimeFactor;
			maxElong=other.maxElong;
			poucheWeight=other.poucheWeight;
			return *this;
		}

		const float bandDiscretisationCount=55.f;
		float bandLen=SIM::milliMeters(200);
		float drawLen=SIM::milliMeters(750);
		float forkWidth=SIM::milliMeters(22*2);
		float poucheWidth=SIM::milliMeters(14*2);
		float bandCount=2;
		float ammoDiameter=SIM::milliMeters(8);
		float ammoMv=7850;
		float slowTimeFactor=0;
		float maxElong=5;
		float poucheWeight=SIM::grames(1.5);
	};
	bool isEnded();

	Simulation(MainWindow* parent,SimSetup ss);
	~Simulation(){}
private:
	struct Results{
		Results():lastFrameIdx(-1),ended(false){}
		~Results(){
			for(auto& f:frames)delete f;
		}
		struct ParticleState{
			SIM::MetersPerSeconds speed;
			SIM::Newton force;
			SIM::M pos;
			SIM::Elongation elongation;//! mean elongation from links attached to it
		};

		struct NoMoreData{};
		using FrameIndex=uint64_t;
		using Frame=std::map<SIM::ParticleID,ParticleState>;
		// the goal is to thread safely let any read from data that already exist
		// while leting the owner put data in the container
		// while being as fast as possible (not thred lock if the data come in faster that it come out)

		void putFrame(Frame&& f){
			frames.push_back(new Frame(std::move(f)));
			lastFrameIdx++;
		}
		const Frame& valueOfFrame(FrameIndex frameID);

		FrameIndex framesAvalable()const {
			return lastFrameIdx;
		}
		bool isEnded()const{return ended;}
		void stop(){ended=true;}
	private:
		std::atomic<FrameIndex> lastFrameIdx;
		std::atomic<FrameIndex> ended;

		//! we put eatch fames on the heap so that the vector resize does not change frames adresses
		//! as we whant to give reference to theme that ares stable in time
		std::vector<Frame*> frames;
	}results;

	std::shared_ptr<SIM::Scene> scene;//! forward declared but totaly owned

	std::map<SIM::ParticleID,QAbstractGraphicsShapeItem*> patricleItems;//! graphics item used to represent particles
	SIM::ParticleID lastID;

	SIM::Seconds elapsed=0;
	SIM::Elongation maxOverallElongation;
	QTimer viewTimer;//! timer that triger simulation update
	uint64_t drawStepCount=0;
	uint64_t simStepCount=0;
	bool launched=false;//! tell if the ammo already passed the forks
};

CEREAL_CLASS_VERSION(Simulation::SimSetup, 2);
