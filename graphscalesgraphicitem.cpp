#include "graphscalesgraphicitem.h"

#include <QColor>
#include <QPainter>


QRectF GraphScalesGraphicItem::boundingRect() const{
	return QRectF(0,0,w,-h);
}

void GraphScalesGraphicItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget){
	painter->setPen(QPen(QColor(Qt::black),0/*cosmetic*/,Qt::PenStyle::DotLine));
	painter->drawLine(0,0,w,0);
	painter->drawLine(0,0,0,-h);
}
